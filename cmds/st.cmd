# This should be a test startup script
require calib_epics
#require e3-common

epicsEnvSet(TOP, "$(E3_CMD_TOP)")

epicsEnvSet("SEC", "SEC")
epicsEnvSet("SUB", "SUB01")
epicsEnvSet("P", "$(SEC)-$(SUB):")
epicsEnvSet("DIS", "DIS")
epicsEnvSet("DEV", "DEV-01")
epicsEnvSet("R", "$(DIS)-$(DEV)")
#epicsEnvSet("IOCNAME", "$(P)$(R)")

epicsEnvSet("IOCNAME", "CalibIOC")
epicsEnvSet("CALIB_IOC_ID", "01")

iocshLoad("$(calib_epics_DIR)/calibration.iocsh")

epicsEnvSet("IOCNAME", "CalibIOC")
epicsEnvSet("CALIB_IOC_ID", "02")

iocshLoad("$(calib_epics_DIR)/calibration.iocsh")
